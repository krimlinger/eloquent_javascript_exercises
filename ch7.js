function compareRobots(robot1, memory1, robot2, memory2) {
  const testVillages = [...Array(100).keys()].map(v => VillageState.random());
  console.log(
    `Robot 1 took ${averageTurns(testVillages, robot1, memory1)} turns.`);
  console.log(
    `Robot 2 took ${averageTurns(testVillages, robot2, memory2)} turns.`);
}

function averageTurns(states, robot, memory) {
  return states.map(s => runRobotNoLog(s, robot, memory))
               .reduce((acc, e) => acc + e, 0) / states.length;
}

function runRobotNoLog(state, robot, memory) {
  let i = 0;
  while (true) {
    if (state.parcels.length == 0)
      break
    let action = robot(state, memory);
    state = state.move(action.direction);
    memory = action.memory;
    i++;
  }
  return i;
}


class PGroup {
  constructor(members=[]) {
    this.members = members;
  }

  add(e) {
    return new PGroup(this.members.concat([e]));
  }

  delete(e) {
    const i = this.members.indexOf(e);
    if (i == -1)
      return this;
    return new PGroup(this.members.slice(0, i)
                                  .concat(this.members.slice(i+1)));
  }

  has(e) {
    return this.members.includes(e);
  }

  static get empty() {
    return new PGroup();
  }
}

