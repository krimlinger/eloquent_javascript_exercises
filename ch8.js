class MultiplicatorUnitFailure extends Error {}

function primitiveMultiply(a, b) {
  if (Math.random() < 0.2) {
    return a * b;
  } else {
    throw new MultiplicatorUnitFailure("Klunk");
  }
}
function reliableMultiply(a, b) {
  while (true) {
    try {
     return primitiveMultiply(a, b);
    } catch (e) {
      if (e instanceof MultiplicatorUnitFailure)
        return reliableMultiply(a, b);
      throw(e);
    }
  }
}

function withBoxUnlocked(fn) {
  let locked = box.locked;
  if (!locked)
    return fn();

  box.unlock();
  try {
    fn();
  } finally {
    box.lock();
  }
}
