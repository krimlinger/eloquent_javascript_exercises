function range(start, end, step = start < end ? 1: -1) {
  let res = Array();
  let comp = step > 0 ? (a,b) => (a<=b): (a,b) => (a>=b);
  if ((start < end && step <= 0) || (start > end && step >= 0))
    return res;
  for (let i = start; comp(i, end); i += step)
    res.push(i);
  return res;
}

function sum(arr) {
  let sum = 0;
  for (let e of arr)
    sum += e;
  return sum;
}

function reverseArray(arr) {
  let res = Array();
  for (let e of arr)
    res.unshift(e);
  return res;
}

function reverseArrayInPlace(arr) {
  let n = arr.length;
  for (let i = 0; i < Math.floor(n/2); i++) {
    let tmp = arr[i];
    arr[i] = arr[n-i-1]
    arr[n-i-1] = tmp;
  }
}


function arrayToList(arr) {
  if (arr.length < 1) {
    console.log("arrayToList need an array of size >= 1");
    return undefined;
  }
  let head = { value: arr[0], rest: null };
  let curr = head;
  for (let e of arr.slice(1)) {
    let next = { value: e, rest: null };
    curr.rest = next;
    curr = next;
  }
  return head;
}

function listToArray(list) {
  let arr = Array();
  while (list != null) {
    arr.push(list.value);
    list = list.rest;
  }
  return arr;
}

function prepend(element, list) {
  return { value: element, rest: list };
}

function nth(list, i) {
  if (i == 0) return list.value;
  return nth(list, i+1);
}

function deepEqual(a, b) {
  if (a === b)
    return true;

  if (a === null || typeof(a) != "object" ||
      b === null || typeof(b) != "object")
    return a === b;

  let aKeys = Object.keys(a), bKeys = Object.keys(b);

  if (aKeys.length != bKeys.length)
    return false;

  for (let i = 0; i < aKeys.length; i++)
    if (!deepEqual(a[aKeys[i]], b[bKeys[i]]))
      return false;
  return true;
}
