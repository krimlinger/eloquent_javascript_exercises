function looping() {
  for (let i = 0, line = ""; i < 7; i++, line += "#")
    console.log(line);
}

function fizzBuzz() {
  for(let i = 1; i < 101; i++) {
    let msg = "";
    if (i % 3 == 0)
      msg += "Fizz";
    if (i % 5 == 0)
      msg += "Buzz";
    console.log(msg || n);
  }
}

function chessboard(size) {
  for (let i = 0; i < size; i++) {
    let line = "";
    for (let j = 0; j < size; j++) {
      if ((i+j) % 2 != 0)
        line += "#";
      else
        line += " ";
    }
    console.log(line);
  }
}
