class Vec {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }

  plus(v) {
    return new Vec(this.x+v.x, this.y+v.y);
  }

  minus(v) {
    return new Vec(this.x-v.x, this.y-v.y);
  }

  get length() {
    return Math.sqrt(this.x**2 + this.y**2);
  }
}

class Group {
  constructor(members=[]) {
    this.members = members;
  }

  add(e) {
    this.members.push(e);
  }

  delete(e) {
    let i = this.members.indexOf(e);
    if (i != -1)
      this.members.splice(i, 1);
    else
      console.log(`warning: ${e} element not found in Group`);
  }

  has(e) {
    return this.members.includes(e);
  }

  static from(iterable) {
    return new Group(Array.from(iterable));
  }

  [Symbol.iterator]() {
    return new GroupIterator(this);
  }
}

class GroupIterator {
  constructor(group) {
    this.group = group;
    this.index = 0;
    this.lastIndex = group.members.length;
  }

  next() {
    if (this.index == this.lastIndex)
      return {done: true};
    this.index++;
    return {value: this.group.members[this.index-1], done: false};
  }
}

// Borrowing a method
// use: Object.prototype.hasOwnProperty.call(.....)
