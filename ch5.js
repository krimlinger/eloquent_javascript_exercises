function flatten(arr) {
  return arr.reduce((acc, e) => acc.concat(e), []);
}

function loop(value, test, update, body) {
  while (test(value)) {
    body(value);
    value = update(value);
  }
}

function every(arr, pred) {
  for (let e of arr)
    if (!pred(e))
      return false;
  return true;
}

function every2(arr, pred) {
  return !arr.some(e => !pred(e))
}

function dominantDirection(text) {
  const counts = countBy(text, (c) => {
      let script = characterScript(c.charCodeAt(0));
      if (script)
        return script.direction;
      return "undefined direction";
    });

  return counts.reduce((d, e) => e.count > d.count ? e: d,
                       {name: "undefined direction", count: 0}).name;
  }
}
