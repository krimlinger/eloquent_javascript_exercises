function min(a, b) {
  if (a < b) return a;
  else return b;
}

function isEven(n) {
  n = (n > 0) ? n: -n;
  if (n == 0) return true;
  else if (n == 1) return false;
  else return isEven(n-2);
}

function countBs(s) {
  return countChar(s, "B");
}

function countChar(s, c) {
  let counter = 0;
  for (let i = 0; i < s.length; i++)
    if (s[i] == c)
      counter++;
  return counter;
}
